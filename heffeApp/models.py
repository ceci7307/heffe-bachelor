from django.db import models
from django.contrib.auth.models import User
from django.db.models import Avg
from decimal import Decimal

class userProfile (models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    consent = models.BooleanField(default=True)

    @classmethod
    def create(cls, user):
        userprofile = cls()
        userprofile.user = user
        userprofile.save()

    def __str__(self):
        return f"{self.user}"

class beer (models.Model):
    name = models.CharField(max_length=35)
    description = models.CharField(max_length=150)
    imageRoot = models.CharField(max_length=150)
    brewery = models.CharField(max_length=35)
    alcohol = models.FloatField(default = 0)
    servingStyle = models.CharField(max_length=150)

    @property 
    def avgScore(self):
        return rating.objects.filter(beer=self).aggregate(Avg('score'))['score__avg'] or Decimal('0')
    
    def __str__(self):
        return f"{self.name}"

class rating (models.Model):
    beer = models.ForeignKey(beer, on_delete=models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    score = models.IntegerField(default = 0)

    def __str__(self):
        return f"{self.beer}"

class group (models.Model):
    name = models.CharField(max_length=35)
    image = models.ImageField(upload_to='media')

    def __str__(self):
        return f"{self.name}"

class groupMember (models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(group, on_delete=models.PROTECT)

    def __str__(self):
        return f"{self.user}, {self.group}"

class event (models.Model):
    group = models.ForeignKey(group, on_delete=models.PROTECT)
    title = models.CharField(max_length=35)
    text = models.CharField(max_length=150)
    date = models.CharField(max_length=35)

    def __str__(self):
        return f"{self.title}"

class accepted (models.Model):
    event = models.ForeignKey(event, on_delete=models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.event}"

class beerList (models.Model):
    event = models.ForeignKey(event, on_delete=models.PROTECT)
    beer = models.ForeignKey(beer, on_delete=models.PROTECT)

    @property 
    def avgScore(self):
        return beerListRating.objects.filter(beerList=self).aggregate(Avg('score'))['score__avg'] or Decimal('0')

    def __str__(self):
        return f"{self.beer}"

class beerListRating (models.Model):
    beerList = models.ForeignKey(beerList, on_delete=models.PROTECT)
    score = models.IntegerField(default = 0)

    def __str__(self):
        return f"{self.beerList}"

class favoriteBeer (models.Model):
    beer = models.ForeignKey(beer, on_delete=models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.user} {self.beer}"
