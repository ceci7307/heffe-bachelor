from django.apps import AppConfig


class HeffeappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'heffeApp'
