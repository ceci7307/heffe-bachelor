from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from django.test import Client

from .models import userProfile
# Create your tests here.

class AgeTests(TestCase):

    def setUp(self):
        ageContentCheckbox = True
        self.testuser = User.objects.create(username='testuser', password='1X<ISRUkw+tuK')
        self.testuser.save()

        if self.testuser:
            testuser_userprofile = userProfile.objects.create(user=self.testuser, consent=ageContentCheckbox)
            testuser_userprofile.save()
        self.client = Client()

    def test_age_consent(self):
        user = User.objects.get(pk=1)
        userprofile = userProfile.objects.get(user=user)
        expected_object = userprofile.consent
        self.assertEquals(expected_object, True)