from django.contrib import admin
from .models import userProfile, beer, rating, group, groupMember, event, accepted, beerList, beerListRating, favoriteBeer

# Register your models here.
admin.site.register(userProfile)
admin.site.register(beer)
admin.site.register(rating)
admin.site.register(group)
admin.site.register(groupMember)
admin.site.register(event)
admin.site.register(accepted)
admin.site.register(beerList)
admin.site.register(beerListRating)
admin.site.register(favoriteBeer)