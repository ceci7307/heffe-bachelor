from django.contrib import admin
from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('frontpage/', views.frontpage, name='frontpage'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('signup/', views.signup, name='signup'),
    path('my-groups/', views.myGroups, name='myGroups'),
    path('createEvent/', views.createEvent, name='createEvent'),
    path('my-favorites/', views.myFavorites, name='myFavorites'),
    path('rate/', views.rate, name='rate'),
    path('rateGroupBeer/', views.rateGroupBeer, name='rateGroupBeer'),
    path('addFavorite/', views.addFavorite, name='addFavorite'),
    path('removeFavorite/', views.removeFavorite, name='removeFavorite'),
    path('my-profile/', views.myProfile, name='myProfile'),
    path('<int:pk>/group-dashboard/', views.groupDashboard, name='groupDashboard'),
    path('my-ratings/', views.myRatings, name='myRatings'),
    path('<int:pk>/', views.singleview, name='singleview'),
    path('<int:pk>/group-dashboard/rate-beer', views.groupRateBeer, name='groupRateBeer'),
    path('update_password/', views.updatePassword, name='updatePassword'),
    path('createGroup/', views.createGroup, name='createGroup'),
    path('deleteAccount/', views.deleteAccount, name='deleteAccount'),

    ]



