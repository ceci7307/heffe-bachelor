from django.shortcuts import render, get_object_or_404, reverse
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.db.models import Q
from .models import *
from .models import userProfile, beer, group, rating, event, favoriteBeer, groupMember, beerList, beerListRating
from django.contrib.auth import authenticate, login as dj_login, logout as dj_logout

def index(request):
    context = {}
    return render(request, 'pages/index.html', context)


def frontpage(request):
    beers = beer.objects.all()
    groups = groupMember.objects.all().filter(user=request.user)

    context = {
        'beers': beers,
        'groups': groups,


    }
    return render(request, 'pages/Frontpage.html', context)

def login(request):
    context = {}
    if request.method == 'POST':
        email = request.POST['loginEmail']
        password = request.POST['loginPassword']
        user = authenticate(request, username=email, password=password)
        if user:
            dj_login(request, user)
            return HttpResponseRedirect(reverse('frontpage'))

    return render( request, 'pages/Login.html', context)

def singleview(request, pk):
    oneBeer = beer.objects.get(pk=pk)
    context = {
        'beer': oneBeer,
    }

    return render( request, 'pages/Singleview.html', context)

def addFavorite(request):
    if request.method == 'POST':
        pk = request.POST['beerId']
        theBeer = beer.objects.get(pk=pk)
        user = request.user

        favorite = favoriteBeer()
        favorite.beer = theBeer
        favorite.user = user
        favorite.save()
        return HttpResponseRedirect(reverse('myFavorites'))


def rate(request):
    if request.method == 'POST':
        pk = request.POST['beerId']
        theBeer = beer.objects.get(pk=pk)
        user = request.user
        score = request.POST['score']

        rate = rating()
        rate.beer = theBeer
        rate.user = user
        rate.score = score
        rate.save()
        return HttpResponseRedirect(reverse('frontpage'))

def rateGroupBeer(request):
    if request.method == 'POST':
        groupId = request.POST['groupId']

        beerListId = request.POST['beerListId']
        theBeerListBeer = beerList.objects.get(pk=beerListId)
        score = request.POST['score']

        rate = beerListRating()
        rate.beerList = theBeerListBeer
        rate.score = score
        rate.save()
        return HttpResponseRedirect(reverse('groupDashboard', args=(groupId),))

def signup(request):
    context = {}
    if request.method == 'POST':
        first_name = request.POST['signUpFullName']
        email = request.POST['signUpEmail']
        password = request.POST['signUpPassword']
        consent = request.POST['agree']

        if consent == 'True':
            user = User.objects.create_user(email, email, password)
            user.first_name = first_name
            user.save()
            if user:
                userprofile = userProfile()
                userprofile.user = user
                userprofile.consent = True
                userprofile.save()
                return HttpResponseRedirect(reverse('login'))
            else:
                context = {'error': 'Could not create user - try something else'}
        else:
            context ={'error': 'You need to be 16 or older to create a user'}
    return render( request, 'pages/Signup.html', context)

def groupDashboard(request, pk):
    allBeers = beer.objects.all()
    singleGroup = group.objects.get(pk=pk)
    try:
        groupEvents = event.objects.all().filter(group=singleGroup)
        beerListBeers = beerList.objects.all().filter(event__in=groupEvents)
        ratedBeers = beerListRating.objects.all().filter(beerList__in=beerListBeers)
    except event.DoesNotExist:
        groupEvents = None
        beerListBeers = None
        ratedBeers = None
    
    context = {
        'group': singleGroup,
        'events': groupEvents,
        'allBeers': allBeers,
        'beerListBeers' : beerListBeers,
        'beers' : ratedBeers,
    }
    return render( request, 'pages/GroupDashboard.html', context)

def createEvent(request):
    if request.method == 'POST':
        pk = request.POST['groupId']
        theGroup = group.objects.get(pk=pk)
        title = request.POST['title']
        text = request.POST['message']
        date = request.POST['date']

        oneEvent = event()
        oneEvent.group = theGroup
        oneEvent.title = title
        oneEvent.text = text
        oneEvent.date = date
        oneEvent.save()

        if oneEvent:
            beerListBeers = request.POST.getlist('beerPk')
            for el in beerListBeers:
                theBeer = beer.objects.get(pk=el)
                oneBeerList = beerList()
                oneBeerList.event = oneEvent
                oneBeerList.beer = theBeer
                oneBeerList.save()
        return HttpResponseRedirect(reverse('groupDashboard', args=(pk),))

def myGroups(request):
    groups = groupMember.objects.all().filter(user=request.user)
    users = User.objects.all()
    context = {
        'groups': groups,
        'users': users
    }
    return render(request, 'pages/MyGroups.html', context)
      

def createGroup(request):
     if request.method == "POST":
        name = request.POST["groupName"]
        image = request.POST["imageUploade"]

        groupName = group()
        groupName.name = name
        groupName.image = image
        groupName.save()

        if groupName:
            members = request.POST.getlist('userPk')
            for member in members:
                theUser = User.objects.get(pk=member)
                createMember = groupMember()
                createMember.user = theUser
                createMember.group = groupName
                createMember.save()
                   
        return HttpResponseRedirect(reverse('myGroups'))

def groupRateBeer(request, pk):
    rateBeer = beerList.objects.get(pk=pk)
    context = {
        'rateBeer': rateBeer,
    }

    return render( request, 'pages/GroupRateBeer.html', context)


def myFavorites(request):
    myFavorites = favoriteBeer.objects.all().filter(user=request.user)
    context = {
        'favorites': 'true',
        'beers': myFavorites,
    }
    return render( request, 'pages/MyFavorites.html', context)

def removeFavorite(request):
    if request.method == 'POST':
        pk = request.POST['favoriteId']
        theFavBeer = favoriteBeer.objects.get(pk=pk)
        
        theFavBeer.delete()
        return HttpResponseRedirect(reverse('myFavorites'))


def myProfile(request):
    context = {
        'profile': 'true'
    }
    return render( request, 'pages/MyProfile.html', context)
    
def myRatings(request):
    myRatedBeers = rating.objects.all().filter(user=request.user)
    context = {
        'myRatings': 'true',
        'beers': myRatedBeers
    }
    return render( request, 'pages/MyRatings.html', context)

def updatePassword(request):
    if request.method == "POST":
        pk = request.POST['pk']
        user = User.objects.get(pk=pk)
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']

        if user:
            if password == confirm_password:
                user.set_password(password)
                user.save()
    return HttpResponseRedirect(reverse('frontpage'))


def logout(request):
    dj_logout(request)
    return HttpResponseRedirect(reverse('login'))

def deleteAccount(request):
   if request.method == "POST":
      if request.POST['confirm_deletion'] == "DELETE":
            user = authenticate(request, username=request.user.username, password=request.POST['password'])

            if user:
               print(f"Deleting user {user}")
               user.delete()
               return HttpResponseRedirect(reverse('login'))
            else:
               print("fail delete")

   return render(request, 'components/DeleteAccount.html')

