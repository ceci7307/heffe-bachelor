---
layout: default
title: Views
---
A request object is passed to the view function. The request object contains all of the request's information, such as form data, url, cookies, and so on.

## Create a view
1. Go to 'heffeApp/views.py'
2. Create the view function for the requested URL. Add variables to _context_ in order to implement it to the page. _example:_
```python
def frontpage(request):
    beers = beer.objects.all()
    context = {
        'beers': beers,
    }
    return render(request, 'pages/Frontpage.html', context)
```
3. Make sure you have created the URL rounting inside 'heffeApp/urls.py'

### Read more about Models anf field types:
<a href="https://docs.djangoproject.com/en/3.2/topics/http/views/">Django Views Documentation</a>