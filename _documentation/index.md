---
layout: default
title: Welcome
---

# Heffe

Heffe is a web app for beer enthusiasts who want to discover different craft beers. This application can also be used as a platform for beer tastings with a group of friends. Users can create a private group, invite friends to an event and add a list of beers to be tasted. Users can use the platform to rate the beers and keep track of the event's top rated beers.

This is the documentation of Heffe for web developers within the company. This documents how the system work, how it's structured and how to begin developing.


## Dependencies

* NPM
* SCSS
* VSCode
* Git
* Python 3
* Python enviorement
* Django
* pip

## How to get started?

1. Have the latest version of <a href="https://www.python.org/">Python</a> installed
2. Create a Python environment on your computer (See guide underneath)
3. Clone the repository from <a href="https://gitlab.com/ceci7307/heffe-bachelor">GitLab</a>
4. Make sure you have <a href="https://www.npmjs.com/">NPM</a>
5. Intall requirements (See guide underneath)

### Create a Python environment:
We recommend that you create a folder for your environment locally on your computer. The folder in this example is called py-env.
Open the folder and create your python enviorement:
```
$ cd py-env
$ python3 -m venv heffeenv
```
Activate your environment:
```
$ source heffeenv/bin/activate
```
You must have the environment activated while working on the project.

### Install requirements and run the project:
Clone the project from GitLab an follow these steps with your environment activated.
```
$ cd heffeProject
$ pip install -r requirements.txt
```
Run the project:
```
$ python manage.py runserver
```

### Install NPM and run SCSS:
Open a new terminal and run:
```
$ cd heffeProject
$ npm install
$ npm run scss
```