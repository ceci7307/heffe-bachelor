---
layout: default
title: Database
---

This project is using an SQLite database which is connected to the django admin site.

### Login to the database:
Go to <a href="http://127.0.0.1:8000/admin/">http://127.0.0.1:8000/admin/</a> and login with your superuser credentials. 

### Make database migrations:
If you need to migrate your database changes, run:
```bash
$ python manage.py makemigrations
$ python manage.py migrate
```

### Entity Relationship Diagram:
The ERD of Heffe is illustrated in the image below. This should provide a clear overview of the database's connections.
We want to have a normalized database with consistent data, so keep that in mind while working in the database.
Our database currently contains one-to-one and one-to-many relationships.

<img src="{{site.baseurl}}/doc_assets/img/erd.png">

### Read more Django admin site:
<a href="https://docs.djangoproject.com/en/3.2/ref/contrib/admin/">The Django Admin site</a>