---
layout: default
title: Assets
---

All static asstets, like images and icons, can be found inside 'heffeApp/static'. 

The logo is to be found in the root of the assets folder.

## How to add asstes:
1. Create a folder for your assets inside 'heffeApp/static'
2. Insert images or other assets to that folders