---
layout: default
title: Client
---

SASS is used to style the application. You can find all SCSS files inside 'heffeApp/static/scss'.

## Setup

### 1. Run the project
```
$ python manage.py runserver
```

### 2. Install NPM and start watching scss files
```
$ npm install
$ npm run scss
```

## Add a new SCSS file
1. Create the file in 'heffeApp/static/scss'
2. Import the file to 'heffeApp/static/scss/Style.scss'
3. Build the file by running: $npm build scss

### Table of contents
* <a href="{{site.baseurl}}/documentation/views/">Views</a>
* <a href="{{site.baseurl}}/documentation/url/">Url Routing</a>
* <a href="{{site.baseurl}}/documentation/components/">Components</a>
* <a href="{{site.baseurl}}/documentation/assets/">Assets</a>
* <a href="{{site.baseurl}}/documentation/pages/">Pages</a>