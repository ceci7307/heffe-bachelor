---
layout: default
title: URL Routing
---

You need to tell Django how URLs should be routed, and you do that inside 'heffeApp/urls.py'.

### Provide a route to your view:
Here is an example:
```python
...
urlpatterns = [
   path('/frontpage', views.frontpage, name='frontpage'),
   ...
]
```