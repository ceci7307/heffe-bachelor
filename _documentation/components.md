---
layout: default
title: Components
---

All components can be found in 'heffeApp/templates/components'. 
Components are pieces of code that can be reused on other pages. An example is BlueTopbar.html that includes the topbar on frontpage, my-groups, my-ratings and favorites. We modify the title in the topbar on each page. 

### Add a new component:
1. Create your component in 'heffeApp/templates/components'
2. Go to 'heffeApp/templates/pages' and add your component to a page: _Example_ 

```html
    <!-- Top rated Section  -->
    include 'components/Slider.html'
```
Read more about the _include_ tag and how to add variables <a href="https://docs.djangoproject.com/en/3.2/ref/templates/builtins/#include">here.</a>