---
layout: default
title: Models
---

A model class is equivalent to a database table which will be reflected in the database schema. A model object is equivalent to a row in a database table.

### Create a Model
1. Create your model inside 'heffeApp/models.py'. _Example:_
```python
class Beer(models.Model):
   name = models.CharField(max_length=200)
```
In this example, we create a table named _Beer_ with a _name_ object that is a char field (text field) with max. 200 characters. 

2. Register your Model inside 'heffeApp/admin.py':
```python
from django.contrib import admin
from .models import Beer
admin.site.register(Beer)
```

### Read more about Models anf field types:
<a href="https://docs.djangoproject.com/en/3.2/topics/db/models/">Django Models Documentation</a>