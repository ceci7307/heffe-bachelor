---
layout: default
title: Pages
---

You can find all pages inside 'heffeApp/templates/pages'. If a file has its own URL routing, it is considered a page.

## Add a new page
1. Create the page inside 'heffeApp/templates/pages'.
2. Begin the HTML file by adding the 'components/Base.html - Use the _extend_ tag
3. Add the block title
4. Begin the _block content_ tag 
5. Write the code
6. End the file by _including_ the 'components/BottomNavigation.html' and _endblock_
7. Add the page URL to 'heffeApp/urls.py'
8. Create the view inside 'heffeApp/views.py'