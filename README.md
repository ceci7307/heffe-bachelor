# heffe-bachelor

Created by Mille Wienberg og Cecilia Söderblom in connection with our bachelor project at the Web Development program at KEA. 
This project is a prototype of a progressive web app developed in Django (Python) and SASS. 

## Installation
Create you python enviorement:
Create a folder outsite of this project to store you enviorement in fx.
```
py-env
```

Open the folder and create your python enviorement:
```bash
python3 -m venv heffeenv
```

Activate your enviorement:
```python
source heffeenv/bin/activate
```

Install requirements:
Download this repo and go to this project with your envioremnt activated. Run:
```python
pip install -r requirements.txt
```

Run the project:
```python
python manage.py runserver
```

Next, install npm - you need to have Node to install nmp.
Open a new terminal and go to this project. Run:
```bash
npm install
npm run scss
```

## Documentation
Find the full documentation <a href="https://ceci7307.gitlab.io/heffe-bachelor/documentation/">here.</a>

### Add to the documentation:
```bash
gem install bundler
bundle exec jekyll serve
```
Go to: http://127.0.0.1:4000/heffe-bachelor/documentation